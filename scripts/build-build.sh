#!/bin/sh

# apollo config db info
apollo_config_db_url=jdbc:mysql://10.0.20.108:3306/BAOFOO_CGW_APOLLO_CONFIG_BUILD?characterEncoding=utf8
apollo_config_db_username=baofoo
apollo_config_db_password=mandao@64//


# =============== Please do not modify the following content =============== #
cd ..

# package config-service and admin-service
echo "==== starting to build config-service and admin-service ===="

mvn clean package -DskipTests -pl apollo-configservice,apollo-adminservice -am -Dapollo_profile=github -Dspring_datasource_url=$apollo_config_db_url -Dspring_datasource_username=$apollo_config_db_username -Dspring_datasource_password=$apollo_config_db_password

echo "==== building config-service and admin-service finished ===="


