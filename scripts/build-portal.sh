#!/bin/sh

# apollo portal db info
apollo_portal_db_url=jdbc:mysql://10.0.20.108:3306/BAOFOO_CGW_APOLLO_PORTAL?characterEncoding=utf8
apollo_portal_db_username=baofoo
apollo_portal_db_password=mandao@64//

# meta server url, different environments should have different meta server addresses
dev_meta=http://10.0.20.95:8080
fat_meta=http://10.0.20.95:8081
uat_meta=http://10.0.20.95:8082
build_meta=http://10.9.21.34:18080
pro_meta=http://192.168.131.109:8080
META_SERVERS_OPTS="-Duat_meta=$uat_meta -Ddev_meta=$dev_meta -Dbuild_meta=$build_meta -Dfat_meta=$fat_meta"
# =============== Please do not modify the following content =============== #
cd ..

# package config-service and admin-service
echo "==== starting to build portal ===="

mvn clean package -DskipTests -pl apollo-portal -am -Dapollo_profile=github,auth -Dspring_datasource_url=$apollo_portal_db_url -Dspring_datasource_username=$apollo_portal_db_username -Dspring_datasource_password=$apollo_portal_db_password $META_SERVERS_OPTS

echo "==== building portal finished ===="

echo "==== starting to build client ===="

mvn clean deploy -DskipTests -pl apollo-client -am $META_SERVERS_OPTS

echo "==== building client finished ===="

