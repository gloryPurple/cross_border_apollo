#!/bin/sh


# meta server url, different environments should have different meta server addresses
dev_meta=http://10.0.20.95:8080
fat_meta=http://10.0.20.95:8081
uat_meta=http://10.0.20.95:8082
build_meta=http://10.9.21.34:18080
pro_meta=http://192.168.131.109:8080
META_SERVERS_OPTS="-Duat_meta=$uat_meta -Ddev_meta=$dev_meta -Dfat_meta=$fat_meta -Dpro_meta=$pro_meta -Dbuild_meta=$build_meta"
# =============== Please do not modify the following content =============== #
cd ..

# package config-service and admin-service
echo "==== starting to build client ===="

mvn clean deploy -DskipTests -pl apollo-client -am $META_SERVERS_OPTS

echo "==== building client finished ===="

